<?php
/*
Plugin Name: Different Events
Description: Create some event.
Version: 1.0
Author: Pasko Dmitry
*/
?>
<?php
/*  Copyright 2017  Pasko_dmitry  (email: PaskoDima@meta.ua)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
?>
<?php
define('DIFFERENT_EVENTS_DIR', plugin_dir_path(__FILE__));
define('DIFFERENT_EVENTS_URL', plugin_dir_url(__FILE__));

function wpt_event_posttype() {
    register_post_type( 'events',
        array(
            'labels' => array(
                'name' => __( 'События' ),
	            'singular_name' => __( 'Событие' ),
                'add_new' => __( 'Добавить новое' ),
                'add_new_item' => __( 'Добавить новое событие' ),
                'edit_item' => __( 'Редактировать событие' ),
                'new_item' => __( 'Новое событие' ),
                'view_item' => __( 'Посмотреть событие' ),
                'search_items' => __( 'Искать события' ),
                'not_found' => __( 'События не найдены' ),
                'not_found_in_trash' => __( 'В корзине события не найдены' )
	            ),
            'public' => true,
            'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
            'taxonomies' => array( 'taxonomies_for_events' ),
            'menu_icon' => plugins_url( 'events.png', __FILE__ ),
            'capability_type' => 'post',
            'rewrite' => array("slug" => "events"), // Permalinks format
            'menu_position' => 5,
            'register_meta_box_cb' => 'add_events_metaboxes'
        )
    );
}

add_action( 'init', 'wpt_event_posttype' );

add_action( 'add_meta_boxes', 'add_events_metaboxes' );

// Add the Events Meta Boxes


function add_events_metaboxes() {
    add_meta_box( 'wpt_events_status', 'Информация о событии', 'wpt_events_status', 'events', 'normal', 'high' );
}

function wpt_events_status() {
    global $post;

    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

    // Get the location data if its already been entered
    $status = get_post_meta($post->ID, '_status', true);
    $date = get_post_meta($post->ID, '_date', true);
    // Echo out the field
    echo '<p>Введите статус события:</p>';
    echo '<input type="text" name="_status" value="' . $status . '" class="widefat" />';
    echo '<p>Введите дату события:</p>';
    echo '<input type="text" name="_date" value="' . $date  . '" class="widefat" />';
}

// Save the Metabox Data

function wpt_save_events_meta($post_id, $post) {

    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if ( !wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename(__FILE__) )) {
        return $post->ID;
    }

    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ))
        return $post->ID;

    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though.

    $events_meta['_status'] = $_POST['_status'];
    $events_meta['_date'] = $_POST['_date'];
    // Add values of $events_meta as custom fields

    foreach ($events_meta as $key => $value) { // Cycle through the $events_meta array!
        if( $post->post_type == 'revision' ) return; // Don't store custom data twice
        $value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
            update_post_meta($post->ID, $key, $value);

        if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
    }

}

add_action('save_post', 'wpt_save_events_meta', 1, 2); // save the custom fields


add_action( 'init', 'create_my_taxonomies' );

function create_my_taxonomies() {
    register_taxonomy(
        'taxonomies_for_events',
        'events',
        array(
            'labels' => array(
                'name' => 'Type of event',
                'add_new_item' => 'Add New Event Type',
                'new_item_name' => "New Event Type Name"
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true
        )
    );
}

class EventsWidget extends WP_Widget
{
    public function __construct() {
        parent::__construct("events_widget", "Events Widget",
            array("description" => "A simple widget to show how WP Plugins work"));
    }

    public function form($instance) {
        $event_count = "";
        $event_status = "";
        // если instance не пустой, достанем значения
        if (!empty($instance)) {
            $event_count = $instance["event_count"];
            $event_status = $instance["event_status"];
        }

        $tableId = $this->get_field_id("event_count");
        $tableName = $this->get_field_name("event_count");
        echo '<label for="' . $tableId . '">Event count</label><br>';
        echo '<input id="' . $tableId . '" type="text" name="' .
            $tableName . '" value="' . $event_count . '"><br>';

        $statusId = $this->get_field_id("event_status");
        $statusName = $this->get_field_name("event_status");
        echo '<label for="' . $statusId . '">Event status</label><br>';
        echo '<input id="' . $statusId . '" type="text" name="' . $statusName . '" value="' . $event_status . '" ><br>';
    }

    public function update($newInstance, $oldInstance) {
        $values = array();
        $values["event_count"] =  htmlentities($newInstance["event_count"]);
        $values["event_status"] = htmlentities($newInstance["event_status"]);
        return $values;
    }
    public function widget($args, $instance) {

        $event_count = $instance["event_count"];
        $event_status = $instance["event_status"];

        $args = array(
            'post_type' => 'events',
            'posts_per_page' => "$event_count",
            'meta_key' => '_status',
            'meta_value' => "$event_status"
        );
        $query = new WP_Query;
        $my_posts = $query->query($args);

        echo '<h2>События :</h2></br>';
        foreach( $my_posts as $my_post ){
            echo '<strong>'. $my_post->post_title .'</strong></br>';
            echo '<p>'. get_post_meta($my_post->ID, '_date', true) .'</p></br>';
        }
    }
}


add_action("widgets_init", function () {
    register_widget("EventsWidget");
});


function event_shortcode_function($atts) {
    extract(shortcode_atts(array(
        "count" => 3,
        "status" => ('Открытое')
    ), $atts));

    $args = array(
        'post_type' => 'events',
        'posts_per_page' => $atts['count'],
        'meta_key' => '_status',
        'meta_value' => $atts['status']
    );
    $query = new WP_Query;
    $my_posts = $query->query($args);

    echo '<h2>События :</h2></br>';
    foreach( $my_posts as $my_post ){
        echo '<strong>'. $my_post->post_title .'</strong></br>';
        echo '<p>'. get_post_meta($my_post->ID, '_date', true) .'</p>';
    }
}

add_shortcode('eventshortcode', 'event_shortcode_function');
